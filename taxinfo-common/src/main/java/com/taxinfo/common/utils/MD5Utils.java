//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.taxinfo.common.utils;

import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigInteger;
import java.security.MessageDigest;

public class MD5Utils {
    public MD5Utils() {
    }

    public static String createMd5(String param) {
        String md5 = DigestUtils.md5DigestAsHex(param.getBytes());
        return md5;
    }

    public static String getMd5(MultipartFile file) {
        try {
            byte[] uploadBytes = file.getBytes();
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] digest = md5.digest(uploadBytes);
            String hashString = (new BigInteger(1, digest)).toString(16);
            return hashString;
        } catch (Exception var5) {
            System.out.println("校验文件异常>>>>" + var5.toString());
            return null;
        }
    }
}
