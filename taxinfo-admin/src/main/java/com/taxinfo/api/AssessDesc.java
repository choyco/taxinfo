package com.taxinfo.api;

public class AssessDesc {
    private String btnname;
    private boolean itemText;
    private boolean textBox;

    public AssessDesc() {
    }

    public String getBtnname() {
        return this.btnname;
    }

    public void setBtnname(String btnname) {
        this.btnname = btnname;
    }

    public boolean isItemText() {
        return this.itemText;
    }

    public void setItemText(boolean itemText) {
        this.itemText = itemText;
    }

    public boolean isTextBox() {
        return this.textBox;
    }

    public void setTextBox(boolean textBox) {
        this.textBox = textBox;
    }
}
