package com.taxinfo.api;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.utils.MD5Utils;
import com.taxinfo.manager.domain.*;
import com.taxinfo.manager.service.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.tomcat.jni.FileInfo;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;

@RestController
@RequestMapping({"/FBXT_Mybatis"})
public class AdController {

    public Map<String, Object> map = new HashMap();

    @Resource
    private IParamerService paramerService;

    @Resource
    private IPicturefileService picturefileService;

    @Resource
    private IPorfileService porfileService;

    @Resource
    private IEmployeeService employeeService;

    @Resource
    private IAssessService assessService;

    @Resource
    private IFileinfoService fileinfoService;

    @Resource
    private ISystemupdateService systemupdateService;

    /**
     * 员工信息上传
     *
     * @param mould
     * @return
     */
    @RequestMapping({"/service/InterfaceAction_emplist"})
    public Map<String, Object> emplist(String mould) {
        List<Paramer> paramers = paramerService.selectParamerList(new Paramer());
        if (CollectionUtils.isEmpty(paramers)) {
            return AjaxResult.error("系统IP未配置！");
        }
        String IP = paramers.get(0).getIp();
        this.picturefileService.deleteAll();

        JSONObject jsonObj = JSONObject.parseObject(mould);
        List<Picturefile> list = JSONArray.parseArray(jsonObj.getString("list"), Picturefile.class);

        for (Picturefile p : list) {
            String fileName = MD5Utils.createMd5(p.getPicturename() + p.getId());
            String url = "http://" + IP + ":" + "8383" + "/file/download?fileName=" + fileName + ".jpg";
            p.setPictureAddress(url);
            this.picturefileService.insertPicturefile(p);
        }

        this.map.put("data", "");
        this.map.put("code", Msg.success);
        this.map.put("msg", Msg.successMsg);
        return this.map;
    }

    /**
     * 评价器初始化接口 （获取文字大小，颜色，滚动文字大小，颜色，音量，滚动方式，员工信息字体大小，音量）
     *
     * @param id
     * @return
     */
    @RequestMapping({"/service/InterfaceAction_porfile"})
    public Map<String, Object> porfile(String id) {
        Porfile porfile = porfileService.selectPorfileById(1);
        porfile.setAdvertisement(porfile.getOrganid());
        this.map.put("data", porfile);
        this.map.put("code", Msg.success);
        this.map.put("msg", Msg.successMsg);
        return this.map;
    }

    /**
     * 通过设备ID获取对应平板配置信息
     *
     * @param id
     * @return
     */
    @RequestMapping({"/service/InterfaceAction_findEmpByid"})
    public Map<String, Object> findEmpByid(String id) {
        Employee employee = employeeService.findByName(id);
        this.map.put("data", employee);
        this.map.put("code", Msg.success);
        this.map.put("msg", Msg.successMsg);
        return this.map;
    }

    /**
     * 获取评价按钮信息
     *
     * @return
     */
    @RequestMapping({"/service/assessdetailapi"})
    public Map<String, Object> assessdetailapi() {
        HashMap<String, Object> hashMap = new HashMap();
        List<Assess> assessList = assessService.selectAssessList(new Assess());
        List<AssessDesc> assessDescs = new ArrayList();
        new ArrayList();
        String normName = "";

        for (Assess assess : assessList) {
            normName = assess.getNormName();
            AssessDesc assessDesc = new AssessDesc();
            assessDesc.setBtnname(assess.getNormDescribe());
            assessDesc.setItemText(assess.getItemText() == 1);
            assessDesc.setTextBox(assess.getTextBox() == 1);
            assessDescs.add(assessDesc);
            if (!StringUtils.isEmpty(normName)) {
                normName = normName.substring(0, normName.length() - 1);
                assess.setNormName(normName);
            }
        }

        hashMap.put("code", Msg.success);
        hashMap.put("msg", Msg.successMsg);
        hashMap.put("rs", assessDescs);
        hashMap.put("data", assessList);
        return hashMap;
    }

    /**
     *评价器获取员工信息
     * @param pid
     * @return
     */
    @RequestMapping({"/service/InterfaceAction_showEmpByid"})
    public Map<String, Object> showEmpByid(String pid) {
        Picturefile picture = new Picturefile();
        picture.setId(pid);
        Picturefile employee = picturefileService.selectPicturefileById(pid);
        List<Fileinfo> fileInfo = fileinfoService.selectByPath(employee.getPictureAddress());
        if (null == fileInfo) {
            employee.setPictureAddress(null);
        }

        this.map.put("data", employee);
        this.map.put("code", Msg.success);
        this.map.put("msg", Msg.successMsg);
        return this.map;
    }

    /**
     * 获取APK版本
     * @return
     */
    @RequestMapping({"/service/seystemversion"})
    public Map<String, Object> seystemversion() {
        List<Systemupdate> list = systemupdateService.selectSystemupdateList(new Systemupdate());
        if (!list.isEmpty()) {
            this.map.put("data", list.get(0));
            this.map.put("code", Msg.success);
            this.map.put("msg", Msg.successMsg);
        } else {
            this.map.put("code", Msg.success);
            this.map.put("msg", Msg.successMsg);
            this.map.put("data", "");
        }

        return this.map;
    }
}
