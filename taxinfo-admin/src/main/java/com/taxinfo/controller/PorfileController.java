package com.taxinfo.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.taxinfo.common.annotation.Log;
import com.taxinfo.common.core.controller.BaseController;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.enums.BusinessType;
import com.taxinfo.manager.domain.Porfile;
import com.taxinfo.manager.service.IPorfileService;
import com.taxinfo.common.utils.poi.ExcelUtil;
import com.taxinfo.common.core.page.TableDataInfo;

/**
 * 字体设置Controller
 * 
 * @author ycong
 * @date 2023-12-24
 */
@RestController
@RequestMapping("/manager/porfile")
public class PorfileController extends BaseController
{
    @Autowired
    private IPorfileService porfileService;

    /**
     * 查询字体设置列表
     */
    @PreAuthorize("@ss.hasPermi('manager:porfile:list')")
    @GetMapping("/list")
    public TableDataInfo list(Porfile porfile)
    {
        startPage();
        List<Porfile> list = porfileService.selectPorfileList(porfile);
        return getDataTable(list);
    }

    /**
     * 导出字体设置列表
     */
    @PreAuthorize("@ss.hasPermi('manager:porfile:export')")
    @Log(title = "字体设置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Porfile porfile)
    {
        List<Porfile> list = porfileService.selectPorfileList(porfile);
        ExcelUtil<Porfile> util = new ExcelUtil<Porfile>(Porfile.class);
        util.exportExcel(response, list, "字体设置数据");
    }

    /**
     * 获取字体设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('manager:porfile:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(porfileService.selectPorfileById(id));
    }

    /**
     * 新增字体设置
     */
    @PreAuthorize("@ss.hasPermi('manager:porfile:add')")
    @Log(title = "字体设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Porfile porfile)
    {
        return toAjax(porfileService.insertPorfile(porfile));
    }

    /**
     * 修改字体设置
     */
    @PreAuthorize("@ss.hasPermi('manager:porfile:edit')")
    @Log(title = "字体设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Porfile porfile)
    {
        return toAjax(porfileService.updatePorfile(porfile));
    }

    /**
     * 删除字体设置
     */
    @PreAuthorize("@ss.hasPermi('manager:porfile:remove')")
    @Log(title = "字体设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(porfileService.deletePorfileByIds(ids));
    }
}
