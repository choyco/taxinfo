package com.taxinfo.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.taxinfo.common.annotation.Log;
import com.taxinfo.common.core.controller.BaseController;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.enums.BusinessType;
import com.taxinfo.manager.domain.Systemupdate;
import com.taxinfo.manager.service.ISystemupdateService;
import com.taxinfo.common.utils.poi.ExcelUtil;
import com.taxinfo.common.core.page.TableDataInfo;

/**
 * 系统更新Controller
 * 
 * @author ycong
 * @date 2023-12-24
 */
@RestController
@RequestMapping("/manager/systemupdate")
public class SystemupdateController extends BaseController
{
    @Autowired
    private ISystemupdateService systemupdateService;

    /**
     * 查询系统更新列表
     */
    @PreAuthorize("@ss.hasPermi('manager:systemupdate:list')")
    @GetMapping("/list")
    public TableDataInfo list(Systemupdate systemupdate)
    {
        startPage();
        List<Systemupdate> list = systemupdateService.selectSystemupdateList(systemupdate);
        return getDataTable(list);
    }

    /**
     * 导出系统更新列表
     */
    @PreAuthorize("@ss.hasPermi('manager:systemupdate:export')")
    @Log(title = "系统更新", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Systemupdate systemupdate)
    {
        List<Systemupdate> list = systemupdateService.selectSystemupdateList(systemupdate);
        ExcelUtil<Systemupdate> util = new ExcelUtil<Systemupdate>(Systemupdate.class);
        util.exportExcel(response, list, "系统更新数据");
    }

    /**
     * 获取系统更新详细信息
     */
    @PreAuthorize("@ss.hasPermi('manager:systemupdate:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return success(systemupdateService.selectSystemupdateByUuid(uuid));
    }

    /**
     * 新增系统更新
     */
    @PreAuthorize("@ss.hasPermi('manager:systemupdate:add')")
    @Log(title = "系统更新", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Systemupdate systemupdate)
    {
        return toAjax(systemupdateService.insertSystemupdate(systemupdate));
    }

    /**
     * 修改系统更新
     */
    @PreAuthorize("@ss.hasPermi('manager:systemupdate:edit')")
    @Log(title = "系统更新", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Systemupdate systemupdate)
    {
        return toAjax(systemupdateService.updateSystemupdate(systemupdate));
    }

    /**
     * 删除系统更新
     */
    @PreAuthorize("@ss.hasPermi('manager:systemupdate:remove')")
    @Log(title = "系统更新", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(systemupdateService.deleteSystemupdateByUuids(uuids));
    }
}
