package com.taxinfo.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.taxinfo.common.core.controller.BaseController;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.enums.BusinessType;
import com.taxinfo.manager.domain.Log;
import com.taxinfo.manager.service.ILogService;
import com.taxinfo.common.utils.poi.ExcelUtil;
import com.taxinfo.common.core.page.TableDataInfo;

/**
 * 日志信息Controller
 * 
 * @author ycong
 * @date 2023-12-24
 */
@RestController
@RequestMapping("/manager/log")
public class LogController extends BaseController
{
    @Autowired
    private ILogService logService;

    /**
     * 查询日志信息列表
     */
    @PreAuthorize("@ss.hasPermi('manager:log:list')")
    @GetMapping("/list")
    public TableDataInfo list(Log log)
    {
        startPage();
        List<Log> list = logService.selectLogList(log);
        return getDataTable(list);
    }

    /**
     * 导出日志信息列表
     */
    @PreAuthorize("@ss.hasPermi('manager:log:export')")
    @PostMapping("/export")
    public void export(HttpServletResponse response, Log log)
    {
        List<Log> list = logService.selectLogList(log);
        ExcelUtil<Log> util = new ExcelUtil<Log>(Log.class);
        util.exportExcel(response, list, "日志信息数据");
    }

    /**
     * 获取日志信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('manager:log:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return success(logService.selectLogByUuid(uuid));
    }

    /**
     * 新增日志信息
     */
    @PreAuthorize("@ss.hasPermi('manager:log:add')")
    @PostMapping
    public AjaxResult add(@RequestBody Log log)
    {
        return toAjax(logService.insertLog(log));
    }

    /**
     * 修改日志信息
     */
    @PreAuthorize("@ss.hasPermi('manager:log:edit')")
    @PutMapping
    public AjaxResult edit(@RequestBody Log log)
    {
        return toAjax(logService.updateLog(log));
    }

    /**
     * 删除日志信息
     */
    @PreAuthorize("@ss.hasPermi('manager:log:remove')")
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(logService.deleteLogByUuids(uuids));
    }
}
