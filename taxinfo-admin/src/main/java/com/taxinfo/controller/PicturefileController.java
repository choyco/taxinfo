package com.taxinfo.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.taxinfo.common.annotation.Log;
import com.taxinfo.common.core.controller.BaseController;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.enums.BusinessType;
import com.taxinfo.manager.domain.Picturefile;
import com.taxinfo.manager.service.IPicturefileService;
import com.taxinfo.common.utils.poi.ExcelUtil;
import com.taxinfo.common.core.page.TableDataInfo;

/**
 * 员工管理Controller
 * 
 * @author ycong
 * @date 2023-12-24
 */
@RestController
@RequestMapping("/manager/picturefile")
public class PicturefileController extends BaseController
{
    @Autowired
    private IPicturefileService picturefileService;

    /**
     * 查询员工管理列表
     */
    @PreAuthorize("@ss.hasPermi('manager:picturefile:list')")
    @GetMapping("/list")
    public TableDataInfo list(Picturefile picturefile)
    {
        startPage();
        List<Picturefile> list = picturefileService.selectPicturefileList(picturefile);
        return getDataTable(list);
    }

    /**
     * 导出员工管理列表
     */
    @PreAuthorize("@ss.hasPermi('manager:picturefile:export')")
    @Log(title = "员工管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Picturefile picturefile)
    {
        List<Picturefile> list = picturefileService.selectPicturefileList(picturefile);
        ExcelUtil<Picturefile> util = new ExcelUtil<Picturefile>(Picturefile.class);
        util.exportExcel(response, list, "员工管理数据");
    }

    /**
     * 获取员工管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('manager:picturefile:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(picturefileService.selectPicturefileById(id));
    }

    /**
     * 新增员工管理
     */
    @PreAuthorize("@ss.hasPermi('manager:picturefile:add')")
    @Log(title = "员工管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Picturefile picturefile)
    {
        return toAjax(picturefileService.insertPicturefile(picturefile));
    }

    /**
     * 修改员工管理
     */
    @PreAuthorize("@ss.hasPermi('manager:picturefile:edit')")
    @Log(title = "员工管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Picturefile picturefile)
    {
        return toAjax(picturefileService.updatePicturefile(picturefile));
    }

    /**
     * 删除员工管理
     */
    @PreAuthorize("@ss.hasPermi('manager:picturefile:remove')")
    @Log(title = "员工管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(picturefileService.deletePicturefileByIds(ids));
    }
}
