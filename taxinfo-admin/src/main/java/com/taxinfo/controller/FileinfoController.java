package com.taxinfo.controller;

import com.taxinfo.common.annotation.Log;
import com.taxinfo.common.core.controller.BaseController;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.core.page.TableDataInfo;
import com.taxinfo.common.enums.BusinessType;
import com.taxinfo.common.utils.poi.ExcelUtil;
import com.taxinfo.manager.domain.Fileinfo;
import com.taxinfo.manager.service.IFileinfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 上传管理Controller
 *
 * @author ycong
 * @date 2023-12-24
 */
@RestController
@RequestMapping("/manager/fileinfo")
@Slf4j
public class FileinfoController extends BaseController {
    @Autowired
    private IFileinfoService fileinfoService;

    /**
     * 查询设备管理，视频下拉
     */
    @GetMapping("/queryVideoSelect")
    public AjaxResult queryVideoSelect(Fileinfo fileinfo) {
        fileinfo.setFiletype(".mp4");
        List<Fileinfo> list = fileinfoService.selectFileinfoList(fileinfo);
        return AjaxResult.success(list);
    }

    /**
     * 查询上传管理列表
     */
    @PreAuthorize("@ss.hasPermi('manager:fileinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(Fileinfo fileinfo) {
        startPage();
        List<Fileinfo> list = fileinfoService.selectFileinfoList(fileinfo);
        return getDataTable(list);
    }

    /**
     * 导出上传管理列表
     */
    @PreAuthorize("@ss.hasPermi('manager:fileinfo:export')")
    @Log(title = "上传管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Fileinfo fileinfo) {
        List<Fileinfo> list = fileinfoService.selectFileinfoList(fileinfo);
        ExcelUtil<Fileinfo> util = new ExcelUtil<Fileinfo>(Fileinfo.class);
        util.exportExcel(response, list, "上传管理数据");
    }

    /**
     * 获取上传管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('manager:fileinfo:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid) {
        return success(fileinfoService.selectFileinfoByUuid(uuid));
    }

    /**
     * 新增上传管理
     */
    @PreAuthorize("@ss.hasPermi('manager:fileinfo:add')")
    @Log(title = "上传管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Fileinfo fileinfo) {
        return toAjax(fileinfoService.insertFileinfo(fileinfo));
    }

    /**
     * 修改上传管理
     */
    @PreAuthorize("@ss.hasPermi('manager:fileinfo:edit')")
    @Log(title = "上传管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Fileinfo fileinfo) {
        return toAjax(fileinfoService.updateFileinfo(fileinfo));
    }

    /**
     * 删除上传管理
     */
    @PreAuthorize("@ss.hasPermi('manager:fileinfo:remove')")
    @Log(title = "上传管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids) {
        return toAjax(fileinfoService.deleteFileinfoByUuids(uuids));
    }
}
