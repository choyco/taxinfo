package com.taxinfo.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.taxinfo.common.utils.IdUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.taxinfo.common.annotation.Log;
import com.taxinfo.common.core.controller.BaseController;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.enums.BusinessType;
import com.taxinfo.manager.domain.Paramer;
import com.taxinfo.manager.service.IParamerService;
import com.taxinfo.common.utils.poi.ExcelUtil;
import com.taxinfo.common.core.page.TableDataInfo;

/**
 * 系统IP配置Controller
 * 
 * @author ycong
 * @date 2023-12-24
 */
@RestController
@RequestMapping("/manager/paramer")
public class ParamerController extends BaseController
{
    @Autowired
    private IParamerService paramerService;

    /**
     * 查询系统IP配置列表
     */
    @PreAuthorize("@ss.hasPermi('manager:paramer:list')")
    @GetMapping("/list")
    public TableDataInfo list(Paramer paramer)
    {
        startPage();
        List<Paramer> list = paramerService.selectParamerList(paramer);
        return getDataTable(list);
    }

    /**
     * 导出系统IP配置列表
     */
    @PreAuthorize("@ss.hasPermi('manager:paramer:export')")
    @Log(title = "系统IP配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Paramer paramer)
    {
        List<Paramer> list = paramerService.selectParamerList(paramer);
        ExcelUtil<Paramer> util = new ExcelUtil<Paramer>(Paramer.class);
        util.exportExcel(response, list, "系统IP配置数据");
    }

    /**
     * 获取系统IP配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('manager:paramer:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return success(paramerService.selectParamerByUuid(uuid));
    }

    /**
     * 新增系统IP配置
     */
    @PreAuthorize("@ss.hasPermi('manager:paramer:add')")
    @Log(title = "系统IP配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Paramer paramer)
    {
        paramerService.delete();
        paramer.setUuid(IdUtils.uuid());
        return toAjax(paramerService.insertParamer(paramer));
    }

    /**
     * 修改系统IP配置
     */
    @PreAuthorize("@ss.hasPermi('manager:paramer:edit')")
    @Log(title = "系统IP配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Paramer paramer)
    {
        return toAjax(paramerService.updateParamer(paramer));
    }

    /**
     * 删除系统IP配置
     */
    @PreAuthorize("@ss.hasPermi('manager:paramer:remove')")
    @Log(title = "系统IP配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(paramerService.deleteParamerByUuids(uuids));
    }
}
