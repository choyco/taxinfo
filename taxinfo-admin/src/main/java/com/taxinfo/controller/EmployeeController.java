package com.taxinfo.controller;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.taxinfo.common.utils.IdUtils;
import com.taxinfo.manager.domain.Fileinfo;
import com.taxinfo.manager.mapper.FileinfoMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.jni.FileInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.taxinfo.common.annotation.Log;
import com.taxinfo.common.core.controller.BaseController;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.enums.BusinessType;
import com.taxinfo.manager.domain.Employee;
import com.taxinfo.manager.service.IEmployeeService;
import com.taxinfo.common.utils.poi.ExcelUtil;
import com.taxinfo.common.core.page.TableDataInfo;

/**
 * 设备管理Controller
 *
 * @author ycong
 * @date 2023-12-24
 */
@RestController
@RequestMapping("/manager/employee")
public class EmployeeController extends BaseController {
    @Autowired
    private IEmployeeService employeeService;

    @Resource
    private FileinfoMapper fileinfoMapper;

    /**
     * 查询设备管理列表
     */
    @PreAuthorize("@ss.hasPermi('manager:employee:list')")
    @GetMapping("/list")
    public TableDataInfo list(Employee employee) {
        startPage();
        List<Employee> list = employeeService.selectEmployeeList(employee);
        return getDataTable(list);
    }

    /**
     * 导出设备管理列表
     */
    @PreAuthorize("@ss.hasPermi('manager:employee:export')")
    @Log(title = "设备管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Employee employee) {
        List<Employee> list = employeeService.selectEmployeeList(employee);
        ExcelUtil<Employee> util = new ExcelUtil<Employee>(Employee.class);
        util.exportExcel(response, list, "设备管理数据");
    }

    /**
     * 获取设备管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('manager:employee:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return success(employeeService.selectEmployeeById(id));
    }

    /**
     * 新增设备管理
     */
    @PreAuthorize("@ss.hasPermi('manager:employee:add')")
    @Log(title = "设备管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Employee employee) {
        this.setEmployee(employee);
        return toAjax(employeeService.insertEmployee(employee));
    }

    /**
     * 修改设备管理
     */
    @PreAuthorize("@ss.hasPermi('manager:employee:edit')")
    @Log(title = "设备管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Employee employee) {
        this.setEmployee(employee);
        return toAjax(employeeService.updateEmployee(employee));
    }

    /**
     * 填充属性
     * @param employee
     */
    private void setEmployee(Employee employee){
        if (StringUtils.isBlank(employee.getId())) {
            employee.setId(IdUtils.uuid());
        }

        String ew = employee.getEw();
        if (StringUtils.isNotBlank(ew)){
            String[] ews = ew.split(",");
            String addString = "";
            String el = "";
            String md5 = "";
            for (String ewId : ews){
                Fileinfo fileInfo = fileinfoMapper.selectFileinfoByUuid(ewId);
                if (null != fileInfo) {
                    addString = addString + "|" + fileInfo.getFilepath();
                    md5 = md5 + "|" + fileInfo.getMd5();
                    el = el + "," + fileInfo.getFilename();
                }
            }
            employee.setAddr(addString);
            employee.setEh(el);
            employee.setMd5(md5);
        }
    }

    /**
     * 删除设备管理
     */
    @PreAuthorize("@ss.hasPermi('manager:employee:remove')")
    @Log(title = "设备管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(employeeService.deleteEmployeeByIds(ids));
    }
}
