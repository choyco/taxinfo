package com.taxinfo.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.taxinfo.common.annotation.Log;
import com.taxinfo.common.core.controller.BaseController;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.enums.BusinessType;
import com.taxinfo.manager.domain.Assessatr;
import com.taxinfo.manager.service.IAssessatrService;
import com.taxinfo.common.utils.poi.ExcelUtil;
import com.taxinfo.common.core.page.TableDataInfo;

/**
 * 评价内容Controller
 *
 * @author ycong
 * @date 2023-12-24
 */
@RestController
@RequestMapping("/manager/assessatr")
public class AssessatrController extends BaseController {
    @Autowired
    private IAssessatrService assessatrService;

    /**
     * 查询评价内容列表
     */
    @PreAuthorize("@ss.hasPermi('manager:assessatr:list')")
    @GetMapping("/list")
    public TableDataInfo list(Assessatr assessatr) {
        startPage();
        List<Assessatr> list = assessatrService.selectAssessatrList(assessatr);
        return getDataTable(list);
    }

    @GetMapping(value = "/selectOptions")
    public AjaxResult selectOptions() {
        return success(assessatrService.selectAssessatrList(new Assessatr()));
    }

    /**
     * 导出评价内容列表
     */
    @PreAuthorize("@ss.hasPermi('manager:assessatr:export')")
    @Log(title = "评价内容", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Assessatr assessatr) {
        List<Assessatr> list = assessatrService.selectAssessatrList(assessatr);
        ExcelUtil<Assessatr> util = new ExcelUtil<Assessatr>(Assessatr.class);
        util.exportExcel(response, list, "评价内容数据");
    }

    /**
     * 获取评价内容详细信息
     */
    @PreAuthorize("@ss.hasPermi('manager:assessatr:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid) {
        return success(assessatrService.selectAssessatrByUuid(uuid));
    }

    /**
     * 新增评价内容
     */
    @PreAuthorize("@ss.hasPermi('manager:assessatr:add')")
    @Log(title = "评价内容", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Assessatr assessatr) {
        return toAjax(assessatrService.insertAssessatr(assessatr));
    }

    /**
     * 修改评价内容
     */
    @PreAuthorize("@ss.hasPermi('manager:assessatr:edit')")
    @Log(title = "评价内容", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Assessatr assessatr) {
        return toAjax(assessatrService.updateAssessatr(assessatr));
    }

    /**
     * 删除评价内容
     */
    @PreAuthorize("@ss.hasPermi('manager:assessatr:remove')")
    @Log(title = "评价内容", businessType = BusinessType.DELETE)
    @DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids) {
        return toAjax(assessatrService.deleteAssessatrByUuids(uuids));
    }
}
