package com.taxinfo.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.taxinfo.common.annotation.Log;
import com.taxinfo.common.core.controller.BaseController;
import com.taxinfo.common.core.domain.AjaxResult;
import com.taxinfo.common.enums.BusinessType;
import com.taxinfo.manager.domain.Assess;
import com.taxinfo.manager.service.IAssessService;
import com.taxinfo.common.utils.poi.ExcelUtil;
import com.taxinfo.common.core.page.TableDataInfo;

/**
 * 评价设置Controller
 * 
 * @author ycong
 * @date 2023-12-24
 */
@RestController
@RequestMapping("/manager/assess")
public class AssessController extends BaseController
{
    @Autowired
    private IAssessService assessService;

    /**
     * 查询评价设置列表
     */
    @PreAuthorize("@ss.hasPermi('manager:assess:list')")
    @GetMapping("/list")
    public TableDataInfo list(Assess assess)
    {
        startPage();
        List<Assess> list = assessService.selectAssessList(assess);
        return getDataTable(list);
    }

    /**
     * 导出评价设置列表
     */
    @PreAuthorize("@ss.hasPermi('manager:assess:export')")
    @Log(title = "评价设置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Assess assess)
    {
        List<Assess> list = assessService.selectAssessList(assess);
        ExcelUtil<Assess> util = new ExcelUtil<Assess>(Assess.class);
        util.exportExcel(response, list, "评价设置数据");
    }

    /**
     * 获取评价设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('manager:assess:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return success(assessService.selectAssessByUuid(uuid));
    }

    /**
     * 新增评价设置
     */
    @PreAuthorize("@ss.hasPermi('manager:assess:add')")
    @Log(title = "评价设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Assess assess)
    {
        return toAjax(assessService.insertAssess(assess));
    }

    /**
     * 修改评价设置
     */
    @PreAuthorize("@ss.hasPermi('manager:assess:edit')")
    @Log(title = "评价设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Assess assess)
    {
        return toAjax(assessService.updateAssess(assess));
    }

    /**
     * 删除评价设置
     */
    @PreAuthorize("@ss.hasPermi('manager:assess:remove')")
    @Log(title = "评价设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(assessService.deleteAssessByUuids(uuids));
    }
}
