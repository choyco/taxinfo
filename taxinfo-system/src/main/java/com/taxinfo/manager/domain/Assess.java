package com.taxinfo.manager.domain;

import com.taxinfo.common.annotation.Excel;
import com.taxinfo.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 评价设置对象 assess
 * 
 * @author ycong
 * @date 2023-12-24
 */
public class Assess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评价内容 */
    private String normCode;

    /** 选择事项 */
    private String sourceFlag;

    /** 指标名称多个逗号分隔 */
    private String normName;

    /** 满意度名称 */
    @Excel(name = "满意度名称")
    private String normDescribe;

    /** 星级 */
    @Excel(name = "星级")
    private String startLevel;

    /** 是否显示列表 */
    @Excel(name = "是否显示列表")
    private Integer itemText;

    /** 是否显示评价文本框 */
    @Excel(name = "是否显示评价文本框")
    private Integer textBox;

    /** 指标描述标识ID（0:非常满意） */
    private String type;

    /** $column.columnComment */
    private String uuid;

    /** 排序号码 */
    private Long sort;

    /** 是否进行强制评价 */
    private Integer isforce;

    public void setNormCode(String normCode) 
    {
        this.normCode = normCode;
    }

    public String getNormCode() 
    {
        return normCode;
    }
    public void setSourceFlag(String sourceFlag) 
    {
        this.sourceFlag = sourceFlag;
    }

    public String getSourceFlag() 
    {
        return sourceFlag;
    }
    public void setNormName(String normName) 
    {
        this.normName = normName;
    }

    public String getNormName() 
    {
        return normName;
    }
    public void setNormDescribe(String normDescribe) 
    {
        this.normDescribe = normDescribe;
    }

    public String getNormDescribe() 
    {
        return normDescribe;
    }
    public void setStartLevel(String startLevel) 
    {
        this.startLevel = startLevel;
    }

    public String getStartLevel() 
    {
        return startLevel;
    }
    public void setItemText(Integer itemText) 
    {
        this.itemText = itemText;
    }

    public Integer getItemText() 
    {
        return itemText;
    }
    public void setTextBox(Integer textBox) 
    {
        this.textBox = textBox;
    }

    public Integer getTextBox() 
    {
        return textBox;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setIsforce(Integer isforce) 
    {
        this.isforce = isforce;
    }

    public Integer getIsforce() 
    {
        return isforce;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("normCode", getNormCode())
            .append("sourceFlag", getSourceFlag())
            .append("normName", getNormName())
            .append("normDescribe", getNormDescribe())
            .append("startLevel", getStartLevel())
            .append("itemText", getItemText())
            .append("textBox", getTextBox())
            .append("type", getType())
            .append("uuid", getUuid())
            .append("sort", getSort())
            .append("isforce", getIsforce())
            .toString();
    }
}
