package com.taxinfo.manager.domain;

import com.taxinfo.common.annotation.Excel;
import com.taxinfo.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 字体设置对象 porfile
 * 
 * @author ycong
 * @date 2023-12-24
 */
public class Porfile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** [应用到所有]1001&gt;政务中心10011&gt;国税局10010002&gt;地税局 */
    private String organid;

    /** 0主动触发1" &gt;被动触发 */
    private String triggermode;

    /** 0" &gt;播放图片1播放视频 */
    private String adsmode;

    /** 视频/评价_音量调整 */
    @Excel(name = "视频/评价_音量调整")
    private String videovolume;

    /** 欢迎界面停留时间  */
    private String weatherinfo;

    /** 一米线界面停留时间 */
    private String weathercity;

    /** 天气信息 字体大小： */
    private String weatherinfoFontsize;

    /** 天气信息  字体颜色： */
    private String weatherinfofontcolor;

    /** 字体 */
    private String weatherinfoFontName;

    /** 日期_信息字体调整:字号 */
    @Excel(name = "日期_信息字体调整:字号")
    private String nowdatefontsize;

    /** 日期_信息字体调整:颜色 */
    @Excel(name = "日期_信息字体调整:颜色")
    private String nowdatefontcolor;

    /** 字体 */
    private String nowdatefontName;

    /** 员工_信息字体调整:字号 */
    @Excel(name = "员工_信息字体调整:字号")
    private String workerinfofontsize;

    /** 员工_信息字体调整:颜色 */
    @Excel(name = "员工_信息字体调整:颜色")
    private String workerinfofontsizefontcolor;

    /** 字体: */
    private String workerInfofontName;

    /** 滚动_信息字体调整:字号 */
    @Excel(name = "滚动_信息字体调整:字号")
    private String scrollingtextfontsize;

    /** 滚动_信息字体调整:颜色 */
    @Excel(name = "滚动_信息字体调整:颜色")
    private String scrollingtextfontcolor;

    /** 滚动文字字体: */
    private String scrollingtextfontname;

    /** 滚动_信息字体调整:显示方式 */
    @Excel(name = "滚动_信息字体调整:显示方式")
    private String scrollingtexttype;

    private String advertisement;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setOrganid(String organid) 
    {
        this.organid = organid;
    }

    public String getOrganid() 
    {
        return organid;
    }
    public void setTriggermode(String triggermode) 
    {
        this.triggermode = triggermode;
    }

    public String getTriggermode() 
    {
        return triggermode;
    }
    public void setAdsmode(String adsmode) 
    {
        this.adsmode = adsmode;
    }

    public String getAdsmode() 
    {
        return adsmode;
    }
    public void setVideovolume(String videovolume) 
    {
        this.videovolume = videovolume;
    }

    public String getVideovolume() 
    {
        return videovolume;
    }
    public void setWeatherinfo(String weatherinfo) 
    {
        this.weatherinfo = weatherinfo;
    }

    public String getWeatherinfo() 
    {
        return weatherinfo;
    }
    public void setWeathercity(String weathercity) 
    {
        this.weathercity = weathercity;
    }

    public String getWeathercity() 
    {
        return weathercity;
    }
    public void setWeatherinfoFontsize(String weatherinfoFontsize) 
    {
        this.weatherinfoFontsize = weatherinfoFontsize;
    }

    public String getWeatherinfoFontsize() 
    {
        return weatherinfoFontsize;
    }
    public void setWeatherinfofontcolor(String weatherinfofontcolor) 
    {
        this.weatherinfofontcolor = weatherinfofontcolor;
    }

    public String getWeatherinfofontcolor() 
    {
        return weatherinfofontcolor;
    }
    public void setWeatherinfoFontName(String weatherinfoFontName) 
    {
        this.weatherinfoFontName = weatherinfoFontName;
    }

    public String getWeatherinfoFontName() 
    {
        return weatherinfoFontName;
    }
    public void setNowdatefontsize(String nowdatefontsize) 
    {
        this.nowdatefontsize = nowdatefontsize;
    }

    public String getNowdatefontsize() 
    {
        return nowdatefontsize;
    }
    public void setNowdatefontcolor(String nowdatefontcolor) 
    {
        this.nowdatefontcolor = nowdatefontcolor;
    }

    public String getNowdatefontcolor() 
    {
        return nowdatefontcolor;
    }
    public void setNowdatefontName(String nowdatefontName) 
    {
        this.nowdatefontName = nowdatefontName;
    }

    public String getNowdatefontName() 
    {
        return nowdatefontName;
    }
    public void setWorkerinfofontsize(String workerinfofontsize) 
    {
        this.workerinfofontsize = workerinfofontsize;
    }

    public String getWorkerinfofontsize() 
    {
        return workerinfofontsize;
    }
    public void setWorkerinfofontsizefontcolor(String workerinfofontsizefontcolor) 
    {
        this.workerinfofontsizefontcolor = workerinfofontsizefontcolor;
    }

    public String getWorkerinfofontsizefontcolor() 
    {
        return workerinfofontsizefontcolor;
    }
    public void setWorkerInfofontName(String workerInfofontName) 
    {
        this.workerInfofontName = workerInfofontName;
    }

    public String getWorkerInfofontName() 
    {
        return workerInfofontName;
    }
    public void setScrollingtextfontsize(String scrollingtextfontsize) 
    {
        this.scrollingtextfontsize = scrollingtextfontsize;
    }

    public String getScrollingtextfontsize() 
    {
        return scrollingtextfontsize;
    }
    public void setScrollingtextfontcolor(String scrollingtextfontcolor) 
    {
        this.scrollingtextfontcolor = scrollingtextfontcolor;
    }

    public String getScrollingtextfontcolor() 
    {
        return scrollingtextfontcolor;
    }
    public void setScrollingtextfontname(String scrollingtextfontname) 
    {
        this.scrollingtextfontname = scrollingtextfontname;
    }

    public String getScrollingtextfontname() 
    {
        return scrollingtextfontname;
    }
    public void setScrollingtexttype(String scrollingtexttype) 
    {
        this.scrollingtexttype = scrollingtexttype;
    }

    public String getScrollingtexttype() 
    {
        return scrollingtexttype;
    }

    public String getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(String advertisement) {
        this.advertisement = advertisement;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("organid", getOrganid())
            .append("triggermode", getTriggermode())
            .append("adsmode", getAdsmode())
            .append("videovolume", getVideovolume())
            .append("weatherinfo", getWeatherinfo())
            .append("weathercity", getWeathercity())
            .append("weatherinfoFontsize", getWeatherinfoFontsize())
            .append("weatherinfofontcolor", getWeatherinfofontcolor())
            .append("weatherinfoFontName", getWeatherinfoFontName())
            .append("nowdatefontsize", getNowdatefontsize())
            .append("nowdatefontcolor", getNowdatefontcolor())
            .append("nowdatefontName", getNowdatefontName())
            .append("workerinfofontsize", getWorkerinfofontsize())
            .append("workerinfofontsizefontcolor", getWorkerinfofontsizefontcolor())
            .append("workerInfofontName", getWorkerInfofontName())
            .append("scrollingtextfontsize", getScrollingtextfontsize())
            .append("scrollingtextfontcolor", getScrollingtextfontcolor())
            .append("scrollingtextfontname", getScrollingtextfontname())
            .append("scrollingtexttype", getScrollingtexttype())
            .toString();
    }
}
