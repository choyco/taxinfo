package com.taxinfo.manager.domain;

import com.taxinfo.common.annotation.Excel;
import com.taxinfo.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 评价内容对象 assessatr
 * 
 * @author ycong
 * @date 2023-12-24
 */
public class Assessatr extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** uuid */
    private String uuid;

    /** 评价内容编码 */
    @Excel(name = "评价内容编码")
    private String label;

    /** 评价内容名称 */
    @Excel(name = "评价内容名称")
    private String value;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setLabel(String label) 
    {
        this.label = label;
    }

    public String getLabel() 
    {
        return label;
    }
    public void setValue(String value) 
    {
        this.value = value;
    }

    public String getValue() 
    {
        return value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("label", getLabel())
            .append("value", getValue())
            .toString();
    }
}
