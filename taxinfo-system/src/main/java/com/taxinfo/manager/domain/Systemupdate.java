package com.taxinfo.manager.domain;

import com.taxinfo.common.annotation.Excel;
import com.taxinfo.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 系统更新对象 systemupdate
 * 
 * @author ycong
 * @date 2023-12-24
 */
public class Systemupdate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** uuid */
    private String uuid;

    /** 系统版本 */
    @Excel(name = "系统版本")
    private String version;

    /** 文件地址 */
    @Excel(name = "文件地址")
    private String url;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setVersion(String version) 
    {
        this.version = version;
    }

    public String getVersion() 
    {
        return version;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("version", getVersion())
            .append("url", getUrl())
            .toString();
    }
}
