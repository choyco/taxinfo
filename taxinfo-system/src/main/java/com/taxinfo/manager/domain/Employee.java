package com.taxinfo.manager.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.taxinfo.common.annotation.Excel;
import com.taxinfo.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 设备管理对象 employee
 * 
 * @author ycong
 * @date 2023-12-24
 */
public class Employee extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 设备编号 */
    @Excel(name = "设备编号")
    private String name;

    /** (时间)一米线 */
    private String eid;

    /** 视频ID */
    private String ew;

    /** 视频名称 */
    @Excel(name = "视频名称")
    private String eh;

    /** $column.columnComment */
    private String et;

    /** 广告 */
    @Excel(name = "广告")
    private String el;

    /** $column.columnComment */
    private String fontNum;

    /** 视频地址 */
    private String addr;

    /** $column.columnComment */
    private Date creTime;

    /** $column.columnComment */
    private String picture;

    /** (时间)欢迎界面 */
    private String tid;

    /** $column.columnComment */
    private String depid;

    /** md5校验值 */
    private String md5;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setEid(String eid) 
    {
        this.eid = eid;
    }

    public String getEid() 
    {
        return eid;
    }
    public void setEw(String ew) 
    {
        this.ew = ew;
    }

    public String getEw() 
    {
        return ew;
    }
    public void setEh(String eh) 
    {
        this.eh = eh;
    }

    public String getEh() 
    {
        return eh;
    }
    public void setEt(String et) 
    {
        this.et = et;
    }

    public String getEt() 
    {
        return et;
    }
    public void setEl(String el) 
    {
        this.el = el;
    }

    public String getEl() 
    {
        return el;
    }
    public void setFontNum(String fontNum) 
    {
        this.fontNum = fontNum;
    }

    public String getFontNum() 
    {
        return fontNum;
    }
    public void setAddr(String addr) 
    {
        this.addr = addr;
    }

    public String getAddr() 
    {
        return addr;
    }
    public void setCreTime(Date creTime) 
    {
        this.creTime = creTime;
    }

    public Date getCreTime() 
    {
        return creTime;
    }
    public void setPicture(String picture) 
    {
        this.picture = picture;
    }

    public String getPicture() 
    {
        return picture;
    }
    public void setTid(String tid) 
    {
        this.tid = tid;
    }

    public String getTid() 
    {
        return tid;
    }
    public void setDepid(String depid) 
    {
        this.depid = depid;
    }

    public String getDepid() 
    {
        return depid;
    }
    public void setMd5(String md5) 
    {
        this.md5 = md5;
    }

    public String getMd5() 
    {
        return md5;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("eid", getEid())
            .append("ew", getEw())
            .append("eh", getEh())
            .append("et", getEt())
            .append("el", getEl())
            .append("fontNum", getFontNum())
            .append("addr", getAddr())
            .append("creTime", getCreTime())
            .append("picture", getPicture())
            .append("tid", getTid())
            .append("depid", getDepid())
            .append("md5", getMd5())
            .toString();
    }
}
