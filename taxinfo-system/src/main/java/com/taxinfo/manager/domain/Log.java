package com.taxinfo.manager.domain;

import com.taxinfo.common.annotation.Excel;
import com.taxinfo.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 日志信息对象 log
 * 
 * @author ycong
 * @date 2023-12-24
 */
public class Log extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String uuid;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String methodname;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String classname;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String paramer;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String result;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String detail;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String exceptionmsg;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setMethodname(String methodname) 
    {
        this.methodname = methodname;
    }

    public String getMethodname() 
    {
        return methodname;
    }
    public void setClassname(String classname) 
    {
        this.classname = classname;
    }

    public String getClassname() 
    {
        return classname;
    }
    public void setParamer(String paramer) 
    {
        this.paramer = paramer;
    }

    public String getParamer() 
    {
        return paramer;
    }
    public void setResult(String result) 
    {
        this.result = result;
    }

    public String getResult() 
    {
        return result;
    }
    public void setDetail(String detail) 
    {
        this.detail = detail;
    }

    public String getDetail() 
    {
        return detail;
    }
    public void setExceptionmsg(String exceptionmsg) 
    {
        this.exceptionmsg = exceptionmsg;
    }

    public String getExceptionmsg() 
    {
        return exceptionmsg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("methodname", getMethodname())
            .append("classname", getClassname())
            .append("paramer", getParamer())
            .append("result", getResult())
            .append("detail", getDetail())
            .append("exceptionmsg", getExceptionmsg())
            .toString();
    }
}
