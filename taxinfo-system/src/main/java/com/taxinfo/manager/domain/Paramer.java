package com.taxinfo.manager.domain;

import com.taxinfo.common.annotation.Excel;
import com.taxinfo.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 系统IP配置对象 paramer
 * 
 * @author ycong
 * @date 2023-12-24
 */
public class Paramer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** uuid */
    private String uuid;

    /** 系统IP配置 */
    @Excel(name = "系统IP配置")
    private String ip;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setIp(String ip) 
    {
        this.ip = ip;
    }

    public String getIp() 
    {
        return ip;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("ip", getIp())
            .toString();
    }
}
