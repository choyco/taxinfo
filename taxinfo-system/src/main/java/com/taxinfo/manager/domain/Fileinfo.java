package com.taxinfo.manager.domain;

import com.taxinfo.common.annotation.Excel;
import com.taxinfo.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 上传管理对象 fileinfo
 * 
 * @author ycong
 * @date 2023-12-24
 */
public class Fileinfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** uuid */
    private String uuid;

    /** 文件名 */
    @Excel(name = "文件名")
    private String filename;

    /** 文件地址 */
    @Excel(name = "文件地址")
    private String filepath;

    /** 文件类型 */
    private String filetype;

    /** 文件说明 */
    private String fileinfo;

    /** md5校验值 */
    private String md5;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setFilepath(String filepath) 
    {
        this.filepath = filepath;
    }

    public String getFilepath() 
    {
        return filepath;
    }
    public void setFiletype(String filetype) 
    {
        this.filetype = filetype;
    }

    public String getFiletype() 
    {
        return filetype;
    }
    public void setFileinfo(String fileinfo) 
    {
        this.fileinfo = fileinfo;
    }

    public String getFileinfo() 
    {
        return fileinfo;
    }
    public void setMd5(String md5) 
    {
        this.md5 = md5;
    }

    public String getMd5() 
    {
        return md5;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("filename", getFilename())
            .append("filepath", getFilepath())
            .append("filetype", getFiletype())
            .append("fileinfo", getFileinfo())
            .append("md5", getMd5())
            .toString();
    }
}
