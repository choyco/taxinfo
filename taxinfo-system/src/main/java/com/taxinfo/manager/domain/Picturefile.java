package com.taxinfo.manager.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.taxinfo.common.annotation.Excel;
import com.taxinfo.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 员工管理对象 picturefile
 * 
 * @author ycong
 * @date 2023-12-24
 */
public class Picturefile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 员工图片 */
    private String pictureAddress;

    /** 员工名称 */
    @Excel(name = "员工名称")
    private String picturename;

    /** 员工星级	 */
    @Excel(name = "员工星级	")
    private String pw;

    /** $column.columnComment */
    private String ph;

    /** $column.columnComment */
    private String pl;

    /** 员工图标 */
    @Excel(name = "员工图标")
    private String pt;

    /** $column.columnComment */
    private Date creTime;

    /** $column.columnComment */
    private String pstyle;

    /** $column.columnComment */
    private String pictureid;

    /** 员工工号 */
    @Excel(name = "员工工号")
    private String id;

    /** $column.columnComment */
    private String depid;

    public void setPictureAddress(String pictureAddress) 
    {
        this.pictureAddress = pictureAddress;
    }

    public String getPictureAddress() 
    {
        return pictureAddress;
    }
    public void setPicturename(String picturename) 
    {
        this.picturename = picturename;
    }

    public String getPicturename() 
    {
        return picturename;
    }
    public void setPw(String pw) 
    {
        this.pw = pw;
    }

    public String getPw() 
    {
        return pw;
    }
    public void setPh(String ph) 
    {
        this.ph = ph;
    }

    public String getPh() 
    {
        return ph;
    }
    public void setPl(String pl) 
    {
        this.pl = pl;
    }

    public String getPl() 
    {
        return pl;
    }
    public void setPt(String pt) 
    {
        this.pt = pt;
    }

    public String getPt() 
    {
        return pt;
    }
    public void setCreTime(Date creTime) 
    {
        this.creTime = creTime;
    }

    public Date getCreTime() 
    {
        return creTime;
    }
    public void setPstyle(String pstyle) 
    {
        this.pstyle = pstyle;
    }

    public String getPstyle() 
    {
        return pstyle;
    }
    public void setPictureid(String pictureid) 
    {
        this.pictureid = pictureid;
    }

    public String getPictureid() 
    {
        return pictureid;
    }
    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDepid(String depid) 
    {
        this.depid = depid;
    }

    public String getDepid() 
    {
        return depid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("pictureAddress", getPictureAddress())
            .append("picturename", getPicturename())
            .append("pw", getPw())
            .append("ph", getPh())
            .append("pl", getPl())
            .append("pt", getPt())
            .append("creTime", getCreTime())
            .append("pstyle", getPstyle())
            .append("pictureid", getPictureid())
            .append("id", getId())
            .append("depid", getDepid())
            .toString();
    }
}
