package com.taxinfo.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taxinfo.manager.mapper.PorfileMapper;
import com.taxinfo.manager.domain.Porfile;
import com.taxinfo.manager.service.IPorfileService;

/**
 * 字体设置Service业务层处理
 * 
 * @author ycong
 * @date 2023-12-24
 */
@Service
public class PorfileServiceImpl implements IPorfileService 
{
    @Autowired
    private PorfileMapper porfileMapper;

    /**
     * 查询字体设置
     * 
     * @param id 字体设置主键
     * @return 字体设置
     */
    @Override
    public Porfile selectPorfileById(Integer id)
    {
        return porfileMapper.selectPorfileById(id);
    }

    /**
     * 查询字体设置列表
     * 
     * @param porfile 字体设置
     * @return 字体设置
     */
    @Override
    public List<Porfile> selectPorfileList(Porfile porfile)
    {
        return porfileMapper.selectPorfileList(porfile);
    }

    /**
     * 新增字体设置
     * 
     * @param porfile 字体设置
     * @return 结果
     */
    @Override
    public int insertPorfile(Porfile porfile)
    {
        return porfileMapper.insertPorfile(porfile);
    }

    /**
     * 修改字体设置
     * 
     * @param porfile 字体设置
     * @return 结果
     */
    @Override
    public int updatePorfile(Porfile porfile)
    {
        return porfileMapper.updatePorfile(porfile);
    }

    /**
     * 批量删除字体设置
     * 
     * @param ids 需要删除的字体设置主键
     * @return 结果
     */
    @Override
    public int deletePorfileByIds(Integer[] ids)
    {
        return porfileMapper.deletePorfileByIds(ids);
    }

    /**
     * 删除字体设置信息
     * 
     * @param id 字体设置主键
     * @return 结果
     */
    @Override
    public int deletePorfileById(Integer id)
    {
        return porfileMapper.deletePorfileById(id);
    }
}
