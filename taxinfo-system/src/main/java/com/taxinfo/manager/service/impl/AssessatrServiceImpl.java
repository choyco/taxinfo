package com.taxinfo.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taxinfo.manager.mapper.AssessatrMapper;
import com.taxinfo.manager.domain.Assessatr;
import com.taxinfo.manager.service.IAssessatrService;

/**
 * 评价内容Service业务层处理
 * 
 * @author ycong
 * @date 2023-12-24
 */
@Service
public class AssessatrServiceImpl implements IAssessatrService 
{
    @Autowired
    private AssessatrMapper assessatrMapper;

    /**
     * 查询评价内容
     * 
     * @param uuid 评价内容主键
     * @return 评价内容
     */
    @Override
    public Assessatr selectAssessatrByUuid(String uuid)
    {
        return assessatrMapper.selectAssessatrByUuid(uuid);
    }

    /**
     * 查询评价内容列表
     * 
     * @param assessatr 评价内容
     * @return 评价内容
     */
    @Override
    public List<Assessatr> selectAssessatrList(Assessatr assessatr)
    {
        return assessatrMapper.selectAssessatrList(assessatr);
    }

    /**
     * 新增评价内容
     * 
     * @param assessatr 评价内容
     * @return 结果
     */
    @Override
    public int insertAssessatr(Assessatr assessatr)
    {
        return assessatrMapper.insertAssessatr(assessatr);
    }

    /**
     * 修改评价内容
     * 
     * @param assessatr 评价内容
     * @return 结果
     */
    @Override
    public int updateAssessatr(Assessatr assessatr)
    {
        return assessatrMapper.updateAssessatr(assessatr);
    }

    /**
     * 批量删除评价内容
     * 
     * @param uuids 需要删除的评价内容主键
     * @return 结果
     */
    @Override
    public int deleteAssessatrByUuids(String[] uuids)
    {
        return assessatrMapper.deleteAssessatrByUuids(uuids);
    }

    /**
     * 删除评价内容信息
     * 
     * @param uuid 评价内容主键
     * @return 结果
     */
    @Override
    public int deleteAssessatrByUuid(String uuid)
    {
        return assessatrMapper.deleteAssessatrByUuid(uuid);
    }
}
