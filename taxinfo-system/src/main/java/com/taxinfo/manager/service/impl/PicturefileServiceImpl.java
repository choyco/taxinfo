package com.taxinfo.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taxinfo.manager.mapper.PicturefileMapper;
import com.taxinfo.manager.domain.Picturefile;
import com.taxinfo.manager.service.IPicturefileService;

/**
 * 员工管理Service业务层处理
 * 
 * @author ycong
 * @date 2023-12-24
 */
@Service
public class PicturefileServiceImpl implements IPicturefileService 
{
    @Autowired
    private PicturefileMapper picturefileMapper;

    /**
     * 查询员工管理
     * 
     * @param id 员工管理主键
     * @return 员工管理
     */
    @Override
    public Picturefile selectPicturefileById(String id)
    {
        return picturefileMapper.selectPicturefileById(id);
    }

    /**
     * 查询员工管理列表
     * 
     * @param picturefile 员工管理
     * @return 员工管理
     */
    @Override
    public List<Picturefile> selectPicturefileList(Picturefile picturefile)
    {
        return picturefileMapper.selectPicturefileList(picturefile);
    }

    /**
     * 新增员工管理
     * 
     * @param picturefile 员工管理
     * @return 结果
     */
    @Override
    public int insertPicturefile(Picturefile picturefile)
    {
        return picturefileMapper.insertPicturefile(picturefile);
    }

    /**
     * 修改员工管理
     * 
     * @param picturefile 员工管理
     * @return 结果
     */
    @Override
    public int updatePicturefile(Picturefile picturefile)
    {
        return picturefileMapper.updatePicturefile(picturefile);
    }

    /**
     * 批量删除员工管理
     * 
     * @param ids 需要删除的员工管理主键
     * @return 结果
     */
    @Override
    public int deletePicturefileByIds(String[] ids)
    {
        return picturefileMapper.deletePicturefileByIds(ids);
    }

    /**
     * 删除员工管理信息
     * 
     * @param id 员工管理主键
     * @return 结果
     */
    @Override
    public int deletePicturefileById(String id)
    {
        return picturefileMapper.deletePicturefileById(id);
    }

    @Override
    public void deleteAll() {
        picturefileMapper.deleteAll();
    }
}
