package com.taxinfo.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taxinfo.manager.mapper.LogMapper;
import com.taxinfo.manager.domain.Log;
import com.taxinfo.manager.service.ILogService;

/**
 * 日志信息Service业务层处理
 * 
 * @author ycong
 * @date 2023-12-24
 */
@Service
public class LogServiceImpl implements ILogService 
{
    @Autowired
    private LogMapper logMapper;

    /**
     * 查询日志信息
     * 
     * @param uuid 日志信息主键
     * @return 日志信息
     */
    @Override
    public Log selectLogByUuid(String uuid)
    {
        return logMapper.selectLogByUuid(uuid);
    }

    /**
     * 查询日志信息列表
     * 
     * @param log 日志信息
     * @return 日志信息
     */
    @Override
    public List<Log> selectLogList(Log log)
    {
        return logMapper.selectLogList(log);
    }

    /**
     * 新增日志信息
     * 
     * @param log 日志信息
     * @return 结果
     */
    @Override
    public int insertLog(Log log)
    {
        return logMapper.insertLog(log);
    }

    /**
     * 修改日志信息
     * 
     * @param log 日志信息
     * @return 结果
     */
    @Override
    public int updateLog(Log log)
    {
        return logMapper.updateLog(log);
    }

    /**
     * 批量删除日志信息
     * 
     * @param uuids 需要删除的日志信息主键
     * @return 结果
     */
    @Override
    public int deleteLogByUuids(String[] uuids)
    {
        return logMapper.deleteLogByUuids(uuids);
    }

    /**
     * 删除日志信息信息
     * 
     * @param uuid 日志信息主键
     * @return 结果
     */
    @Override
    public int deleteLogByUuid(String uuid)
    {
        return logMapper.deleteLogByUuid(uuid);
    }
}
