package com.taxinfo.manager.service;

import java.util.List;
import com.taxinfo.manager.domain.Picturefile;

/**
 * 员工管理Service接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface IPicturefileService 
{
    /**
     * 查询员工管理
     * 
     * @param id 员工管理主键
     * @return 员工管理
     */
    public Picturefile selectPicturefileById(String id);

    /**
     * 查询员工管理列表
     * 
     * @param picturefile 员工管理
     * @return 员工管理集合
     */
    public List<Picturefile> selectPicturefileList(Picturefile picturefile);

    /**
     * 新增员工管理
     * 
     * @param picturefile 员工管理
     * @return 结果
     */
    public int insertPicturefile(Picturefile picturefile);

    /**
     * 修改员工管理
     * 
     * @param picturefile 员工管理
     * @return 结果
     */
    public int updatePicturefile(Picturefile picturefile);

    /**
     * 批量删除员工管理
     * 
     * @param ids 需要删除的员工管理主键集合
     * @return 结果
     */
    public int deletePicturefileByIds(String[] ids);

    /**
     * 删除员工管理信息
     * 
     * @param id 员工管理主键
     * @return 结果
     */
    public int deletePicturefileById(String id);

    void deleteAll();
}
