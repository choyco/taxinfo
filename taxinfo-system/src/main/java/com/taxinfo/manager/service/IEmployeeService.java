package com.taxinfo.manager.service;

import java.util.List;
import com.taxinfo.manager.domain.Employee;

/**
 * 设备管理Service接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface IEmployeeService 
{
    /**
     * 查询设备管理
     * 
     * @param id 设备管理主键
     * @return 设备管理
     */
    public Employee selectEmployeeById(String id);

    /**
     * 查询设备管理列表
     * 
     * @param employee 设备管理
     * @return 设备管理集合
     */
    public List<Employee> selectEmployeeList(Employee employee);

    /**
     * 新增设备管理
     * 
     * @param employee 设备管理
     * @return 结果
     */
    public int insertEmployee(Employee employee);

    /**
     * 修改设备管理
     * 
     * @param employee 设备管理
     * @return 结果
     */
    public int updateEmployee(Employee employee);

    /**
     * 批量删除设备管理
     * 
     * @param ids 需要删除的设备管理主键集合
     * @return 结果
     */
    public int deleteEmployeeByIds(String[] ids);

    /**
     * 删除设备管理信息
     * 
     * @param id 设备管理主键
     * @return 结果
     */
    public int deleteEmployeeById(String id);

    Employee findByName(String id);
}
