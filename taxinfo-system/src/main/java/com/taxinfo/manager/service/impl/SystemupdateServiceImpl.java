package com.taxinfo.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taxinfo.manager.mapper.SystemupdateMapper;
import com.taxinfo.manager.domain.Systemupdate;
import com.taxinfo.manager.service.ISystemupdateService;

/**
 * 系统更新Service业务层处理
 * 
 * @author ycong
 * @date 2023-12-24
 */
@Service
public class SystemupdateServiceImpl implements ISystemupdateService 
{
    @Autowired
    private SystemupdateMapper systemupdateMapper;

    /**
     * 查询系统更新
     * 
     * @param uuid 系统更新主键
     * @return 系统更新
     */
    @Override
    public Systemupdate selectSystemupdateByUuid(String uuid)
    {
        return systemupdateMapper.selectSystemupdateByUuid(uuid);
    }

    /**
     * 查询系统更新列表
     * 
     * @param systemupdate 系统更新
     * @return 系统更新
     */
    @Override
    public List<Systemupdate> selectSystemupdateList(Systemupdate systemupdate)
    {
        return systemupdateMapper.selectSystemupdateList(systemupdate);
    }

    /**
     * 新增系统更新
     * 
     * @param systemupdate 系统更新
     * @return 结果
     */
    @Override
    public int insertSystemupdate(Systemupdate systemupdate)
    {
        return systemupdateMapper.insertSystemupdate(systemupdate);
    }

    /**
     * 修改系统更新
     * 
     * @param systemupdate 系统更新
     * @return 结果
     */
    @Override
    public int updateSystemupdate(Systemupdate systemupdate)
    {
        return systemupdateMapper.updateSystemupdate(systemupdate);
    }

    /**
     * 批量删除系统更新
     * 
     * @param uuids 需要删除的系统更新主键
     * @return 结果
     */
    @Override
    public int deleteSystemupdateByUuids(String[] uuids)
    {
        return systemupdateMapper.deleteSystemupdateByUuids(uuids);
    }

    /**
     * 删除系统更新信息
     * 
     * @param uuid 系统更新主键
     * @return 结果
     */
    @Override
    public int deleteSystemupdateByUuid(String uuid)
    {
        return systemupdateMapper.deleteSystemupdateByUuid(uuid);
    }

    @Override
    public void deleteAll() {
        systemupdateMapper.deleteAll();
    }
}
