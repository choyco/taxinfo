package com.taxinfo.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taxinfo.manager.mapper.EmployeeMapper;
import com.taxinfo.manager.domain.Employee;
import com.taxinfo.manager.service.IEmployeeService;

/**
 * 设备管理Service业务层处理
 * 
 * @author ycong
 * @date 2023-12-24
 */
@Service
public class EmployeeServiceImpl implements IEmployeeService 
{
    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 查询设备管理
     * 
     * @param id 设备管理主键
     * @return 设备管理
     */
    @Override
    public Employee selectEmployeeById(String id)
    {
        return employeeMapper.selectEmployeeById(id);
    }

    /**
     * 查询设备管理列表
     * 
     * @param employee 设备管理
     * @return 设备管理
     */
    @Override
    public List<Employee> selectEmployeeList(Employee employee)
    {
        return employeeMapper.selectEmployeeList(employee);
    }

    /**
     * 新增设备管理
     * 
     * @param employee 设备管理
     * @return 结果
     */
    @Override
    public int insertEmployee(Employee employee)
    {
        return employeeMapper.insertEmployee(employee);
    }

    /**
     * 修改设备管理
     * 
     * @param employee 设备管理
     * @return 结果
     */
    @Override
    public int updateEmployee(Employee employee)
    {
        return employeeMapper.updateEmployee(employee);
    }

    /**
     * 批量删除设备管理
     * 
     * @param ids 需要删除的设备管理主键
     * @return 结果
     */
    @Override
    public int deleteEmployeeByIds(String[] ids)
    {
        return employeeMapper.deleteEmployeeByIds(ids);
    }

    /**
     * 删除设备管理信息
     * 
     * @param id 设备管理主键
     * @return 结果
     */
    @Override
    public int deleteEmployeeById(String id)
    {
        return employeeMapper.deleteEmployeeById(id);
    }

    @Override
    public Employee findByName(String id) {
        return employeeMapper.findByName(id);
    }
}
