package com.taxinfo.manager.service;

import java.util.List;
import com.taxinfo.manager.domain.Systemupdate;

/**
 * 系统更新Service接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface ISystemupdateService 
{
    /**
     * 查询系统更新
     * 
     * @param uuid 系统更新主键
     * @return 系统更新
     */
    public Systemupdate selectSystemupdateByUuid(String uuid);

    /**
     * 查询系统更新列表
     * 
     * @param systemupdate 系统更新
     * @return 系统更新集合
     */
    public List<Systemupdate> selectSystemupdateList(Systemupdate systemupdate);

    /**
     * 新增系统更新
     * 
     * @param systemupdate 系统更新
     * @return 结果
     */
    public int insertSystemupdate(Systemupdate systemupdate);

    /**
     * 修改系统更新
     * 
     * @param systemupdate 系统更新
     * @return 结果
     */
    public int updateSystemupdate(Systemupdate systemupdate);

    /**
     * 批量删除系统更新
     * 
     * @param uuids 需要删除的系统更新主键集合
     * @return 结果
     */
    public int deleteSystemupdateByUuids(String[] uuids);

    /**
     * 删除系统更新信息
     * 
     * @param uuid 系统更新主键
     * @return 结果
     */
    public int deleteSystemupdateByUuid(String uuid);

    void deleteAll();
}
