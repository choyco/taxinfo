package com.taxinfo.manager.service;

import java.util.List;
import com.taxinfo.manager.domain.Assess;

/**
 * 评价设置Service接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface IAssessService 
{
    /**
     * 查询评价设置
     * 
     * @param uuid 评价设置主键
     * @return 评价设置
     */
    public Assess selectAssessByUuid(String uuid);

    /**
     * 查询评价设置列表
     * 
     * @param assess 评价设置
     * @return 评价设置集合
     */
    public List<Assess> selectAssessList(Assess assess);

    /**
     * 新增评价设置
     * 
     * @param assess 评价设置
     * @return 结果
     */
    public int insertAssess(Assess assess);

    /**
     * 修改评价设置
     * 
     * @param assess 评价设置
     * @return 结果
     */
    public int updateAssess(Assess assess);

    /**
     * 批量删除评价设置
     * 
     * @param uuids 需要删除的评价设置主键集合
     * @return 结果
     */
    public int deleteAssessByUuids(String[] uuids);

    /**
     * 删除评价设置信息
     * 
     * @param uuid 评价设置主键
     * @return 结果
     */
    public int deleteAssessByUuid(String uuid);
}
