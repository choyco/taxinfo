package com.taxinfo.manager.service;

import java.util.List;
import com.taxinfo.manager.domain.Porfile;

/**
 * 字体设置Service接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface IPorfileService 
{
    /**
     * 查询字体设置
     * 
     * @param id 字体设置主键
     * @return 字体设置
     */
    public Porfile selectPorfileById(Integer id);

    /**
     * 查询字体设置列表
     * 
     * @param porfile 字体设置
     * @return 字体设置集合
     */
    public List<Porfile> selectPorfileList(Porfile porfile);

    /**
     * 新增字体设置
     * 
     * @param porfile 字体设置
     * @return 结果
     */
    public int insertPorfile(Porfile porfile);

    /**
     * 修改字体设置
     * 
     * @param porfile 字体设置
     * @return 结果
     */
    public int updatePorfile(Porfile porfile);

    /**
     * 批量删除字体设置
     * 
     * @param ids 需要删除的字体设置主键集合
     * @return 结果
     */
    public int deletePorfileByIds(Integer[] ids);

    /**
     * 删除字体设置信息
     * 
     * @param id 字体设置主键
     * @return 结果
     */
    public int deletePorfileById(Integer id);
}
