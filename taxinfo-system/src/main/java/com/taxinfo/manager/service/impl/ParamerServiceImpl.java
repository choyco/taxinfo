package com.taxinfo.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taxinfo.manager.mapper.ParamerMapper;
import com.taxinfo.manager.domain.Paramer;
import com.taxinfo.manager.service.IParamerService;

/**
 * 系统IP配置Service业务层处理
 * 
 * @author ycong
 * @date 2023-12-24
 */
@Service
public class ParamerServiceImpl implements IParamerService 
{
    @Autowired
    private ParamerMapper paramerMapper;

    /**
     * 查询系统IP配置
     * 
     * @param uuid 系统IP配置主键
     * @return 系统IP配置
     */
    @Override
    public Paramer selectParamerByUuid(String uuid)
    {
        return paramerMapper.selectParamerByUuid(uuid);
    }

    /**
     * 查询系统IP配置列表
     * 
     * @param paramer 系统IP配置
     * @return 系统IP配置
     */
    @Override
    public List<Paramer> selectParamerList(Paramer paramer)
    {
        return paramerMapper.selectParamerList(paramer);
    }

    /**
     * 新增系统IP配置
     * 
     * @param paramer 系统IP配置
     * @return 结果
     */
    @Override
    public int insertParamer(Paramer paramer)
    {
        return paramerMapper.insertParamer(paramer);
    }

    /**
     * 修改系统IP配置
     * 
     * @param paramer 系统IP配置
     * @return 结果
     */
    @Override
    public int updateParamer(Paramer paramer)
    {
        return paramerMapper.updateParamer(paramer);
    }

    /**
     * 批量删除系统IP配置
     * 
     * @param uuids 需要删除的系统IP配置主键
     * @return 结果
     */
    @Override
    public int deleteParamerByUuids(String[] uuids)
    {
        return paramerMapper.deleteParamerByUuids(uuids);
    }

    /**
     * 删除系统IP配置信息
     * 
     * @param uuid 系统IP配置主键
     * @return 结果
     */
    @Override
    public int deleteParamerByUuid(String uuid)
    {
        return paramerMapper.deleteParamerByUuid(uuid);
    }

    @Override
    public void delete() {
        paramerMapper.delete();
    }
}
