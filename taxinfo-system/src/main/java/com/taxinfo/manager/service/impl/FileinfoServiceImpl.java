package com.taxinfo.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taxinfo.manager.mapper.FileinfoMapper;
import com.taxinfo.manager.domain.Fileinfo;
import com.taxinfo.manager.service.IFileinfoService;

/**
 * 上传管理Service业务层处理
 * 
 * @author ycong
 * @date 2023-12-24
 */
@Service
public class FileinfoServiceImpl implements IFileinfoService 
{
    @Autowired
    private FileinfoMapper fileinfoMapper;

    /**
     * 查询上传管理
     * 
     * @param uuid 上传管理主键
     * @return 上传管理
     */
    @Override
    public Fileinfo selectFileinfoByUuid(String uuid)
    {
        return fileinfoMapper.selectFileinfoByUuid(uuid);
    }

    /**
     * 查询上传管理列表
     * 
     * @param fileinfo 上传管理
     * @return 上传管理
     */
    @Override
    public List<Fileinfo> selectFileinfoList(Fileinfo fileinfo)
    {
        return fileinfoMapper.selectFileinfoList(fileinfo);
    }

    /**
     * 新增上传管理
     * 
     * @param fileinfo 上传管理
     * @return 结果
     */
    @Override
    public int insertFileinfo(Fileinfo fileinfo)
    {
        return fileinfoMapper.insertFileinfo(fileinfo);
    }

    /**
     * 修改上传管理
     * 
     * @param fileinfo 上传管理
     * @return 结果
     */
    @Override
    public int updateFileinfo(Fileinfo fileinfo)
    {
        return fileinfoMapper.updateFileinfo(fileinfo);
    }

    /**
     * 批量删除上传管理
     * 
     * @param uuids 需要删除的上传管理主键
     * @return 结果
     */
    @Override
    public int deleteFileinfoByUuids(String[] uuids)
    {
        return fileinfoMapper.deleteFileinfoByUuids(uuids);
    }

    /**
     * 删除上传管理信息
     * 
     * @param uuid 上传管理主键
     * @return 结果
     */
    @Override
    public int deleteFileinfoByUuid(String uuid)
    {
        return fileinfoMapper.deleteFileinfoByUuid(uuid);
    }

    @Override
    public List<Fileinfo> selectByPath(String pictureAddress) {
        return fileinfoMapper.selectByPath(pictureAddress);
    }
}
