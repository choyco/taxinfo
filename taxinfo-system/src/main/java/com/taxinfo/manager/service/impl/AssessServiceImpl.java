package com.taxinfo.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taxinfo.manager.mapper.AssessMapper;
import com.taxinfo.manager.domain.Assess;
import com.taxinfo.manager.service.IAssessService;

/**
 * 评价设置Service业务层处理
 * 
 * @author ycong
 * @date 2023-12-24
 */
@Service
public class AssessServiceImpl implements IAssessService 
{
    @Autowired
    private AssessMapper assessMapper;

    /**
     * 查询评价设置
     * 
     * @param uuid 评价设置主键
     * @return 评价设置
     */
    @Override
    public Assess selectAssessByUuid(String uuid)
    {
        return assessMapper.selectAssessByUuid(uuid);
    }

    /**
     * 查询评价设置列表
     * 
     * @param assess 评价设置
     * @return 评价设置
     */
    @Override
    public List<Assess> selectAssessList(Assess assess)
    {
        return assessMapper.selectAssessList(assess);
    }

    /**
     * 新增评价设置
     * 
     * @param assess 评价设置
     * @return 结果
     */
    @Override
    public int insertAssess(Assess assess)
    {
        return assessMapper.insertAssess(assess);
    }

    /**
     * 修改评价设置
     * 
     * @param assess 评价设置
     * @return 结果
     */
    @Override
    public int updateAssess(Assess assess)
    {
        return assessMapper.updateAssess(assess);
    }

    /**
     * 批量删除评价设置
     * 
     * @param uuids 需要删除的评价设置主键
     * @return 结果
     */
    @Override
    public int deleteAssessByUuids(String[] uuids)
    {
        return assessMapper.deleteAssessByUuids(uuids);
    }

    /**
     * 删除评价设置信息
     * 
     * @param uuid 评价设置主键
     * @return 结果
     */
    @Override
    public int deleteAssessByUuid(String uuid)
    {
        return assessMapper.deleteAssessByUuid(uuid);
    }
}
