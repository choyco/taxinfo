package com.taxinfo.manager.mapper;

import java.util.List;
import com.taxinfo.manager.domain.Log;

/**
 * 日志信息Mapper接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface LogMapper 
{
    /**
     * 查询日志信息
     * 
     * @param uuid 日志信息主键
     * @return 日志信息
     */
    public Log selectLogByUuid(String uuid);

    /**
     * 查询日志信息列表
     * 
     * @param log 日志信息
     * @return 日志信息集合
     */
    public List<Log> selectLogList(Log log);

    /**
     * 新增日志信息
     * 
     * @param log 日志信息
     * @return 结果
     */
    public int insertLog(Log log);

    /**
     * 修改日志信息
     * 
     * @param log 日志信息
     * @return 结果
     */
    public int updateLog(Log log);

    /**
     * 删除日志信息
     * 
     * @param uuid 日志信息主键
     * @return 结果
     */
    public int deleteLogByUuid(String uuid);

    /**
     * 批量删除日志信息
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLogByUuids(String[] uuids);
}
