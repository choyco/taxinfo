package com.taxinfo.manager.mapper;

import java.util.List;
import com.taxinfo.manager.domain.Fileinfo;

/**
 * 上传管理Mapper接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface FileinfoMapper 
{
    /**
     * 查询上传管理
     * 
     * @param uuid 上传管理主键
     * @return 上传管理
     */
    public Fileinfo selectFileinfoByUuid(String uuid);

    /**
     * 查询上传管理列表
     * 
     * @param fileinfo 上传管理
     * @return 上传管理集合
     */
    public List<Fileinfo> selectFileinfoList(Fileinfo fileinfo);

    /**
     * 新增上传管理
     * 
     * @param fileinfo 上传管理
     * @return 结果
     */
    public int insertFileinfo(Fileinfo fileinfo);

    /**
     * 修改上传管理
     * 
     * @param fileinfo 上传管理
     * @return 结果
     */
    public int updateFileinfo(Fileinfo fileinfo);

    /**
     * 删除上传管理
     * 
     * @param uuid 上传管理主键
     * @return 结果
     */
    public int deleteFileinfoByUuid(String uuid);

    /**
     * 批量删除上传管理
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFileinfoByUuids(String[] uuids);

    List<Fileinfo> selectByPath(String pictureAddress);
}
