package com.taxinfo.manager.mapper;

import java.util.List;
import com.taxinfo.manager.domain.Paramer;

/**
 * 系统IP配置Mapper接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface ParamerMapper 
{
    /**
     * 查询系统IP配置
     * 
     * @param uuid 系统IP配置主键
     * @return 系统IP配置
     */
    public Paramer selectParamerByUuid(String uuid);

    /**
     * 查询系统IP配置列表
     * 
     * @param paramer 系统IP配置
     * @return 系统IP配置集合
     */
    public List<Paramer> selectParamerList(Paramer paramer);

    /**
     * 新增系统IP配置
     * 
     * @param paramer 系统IP配置
     * @return 结果
     */
    public int insertParamer(Paramer paramer);

    /**
     * 修改系统IP配置
     * 
     * @param paramer 系统IP配置
     * @return 结果
     */
    public int updateParamer(Paramer paramer);

    /**
     * 删除系统IP配置
     * 
     * @param uuid 系统IP配置主键
     * @return 结果
     */
    public int deleteParamerByUuid(String uuid);

    /**
     * 批量删除系统IP配置
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteParamerByUuids(String[] uuids);

    void delete();
}
