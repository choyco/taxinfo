package com.taxinfo.manager.mapper;

import java.util.List;
import com.taxinfo.manager.domain.Assessatr;

/**
 * 评价内容Mapper接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface AssessatrMapper 
{
    /**
     * 查询评价内容
     * 
     * @param uuid 评价内容主键
     * @return 评价内容
     */
    public Assessatr selectAssessatrByUuid(String uuid);

    /**
     * 查询评价内容列表
     * 
     * @param assessatr 评价内容
     * @return 评价内容集合
     */
    public List<Assessatr> selectAssessatrList(Assessatr assessatr);

    /**
     * 新增评价内容
     * 
     * @param assessatr 评价内容
     * @return 结果
     */
    public int insertAssessatr(Assessatr assessatr);

    /**
     * 修改评价内容
     * 
     * @param assessatr 评价内容
     * @return 结果
     */
    public int updateAssessatr(Assessatr assessatr);

    /**
     * 删除评价内容
     * 
     * @param uuid 评价内容主键
     * @return 结果
     */
    public int deleteAssessatrByUuid(String uuid);

    /**
     * 批量删除评价内容
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAssessatrByUuids(String[] uuids);
}
