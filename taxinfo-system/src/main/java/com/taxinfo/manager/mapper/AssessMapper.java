package com.taxinfo.manager.mapper;

import java.util.List;
import com.taxinfo.manager.domain.Assess;

/**
 * 评价设置Mapper接口
 * 
 * @author ycong
 * @date 2023-12-24
 */
public interface AssessMapper 
{
    /**
     * 查询评价设置
     * 
     * @param uuid 评价设置主键
     * @return 评价设置
     */
    public Assess selectAssessByUuid(String uuid);

    /**
     * 查询评价设置列表
     * 
     * @param assess 评价设置
     * @return 评价设置集合
     */
    public List<Assess> selectAssessList(Assess assess);

    /**
     * 新增评价设置
     * 
     * @param assess 评价设置
     * @return 结果
     */
    public int insertAssess(Assess assess);

    /**
     * 修改评价设置
     * 
     * @param assess 评价设置
     * @return 结果
     */
    public int updateAssess(Assess assess);

    /**
     * 删除评价设置
     * 
     * @param uuid 评价设置主键
     * @return 结果
     */
    public int deleteAssessByUuid(String uuid);

    /**
     * 批量删除评价设置
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAssessByUuids(String[] uuids);
}
