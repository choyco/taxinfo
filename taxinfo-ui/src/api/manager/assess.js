import request from '@/utils/request'

// 查询评价设置列表
export function listAssess(query) {
  return request({
    url: '/manager/assess/list',
    method: 'get',
    params: query
  })
}

// 查询评价设置详细
export function getAssess(uuid) {
  return request({
    url: '/manager/assess/' + uuid,
    method: 'get'
  })
}

// 新增评价设置
export function addAssess(data) {
  return request({
    url: '/manager/assess',
    method: 'post',
    data: data
  })
}

// 修改评价设置
export function updateAssess(data) {
  return request({
    url: '/manager/assess',
    method: 'put',
    data: data
  })
}

// 删除评价设置
export function delAssess(uuid) {
  return request({
    url: '/manager/assess/' + uuid,
    method: 'delete'
  })
}

// 查询评价内容下拉选择
export function selectOptions() {
  return request({
    url: '/manager/assessatr/selectOptions',
    method: 'get'
  })
}

