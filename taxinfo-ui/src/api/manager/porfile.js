import request from '@/utils/request'

// 查询字体设置列表
export function listPorfile(query) {
  return request({
    url: '/manager/porfile/list',
    method: 'get',
    params: query
  })
}

// 查询字体设置详细
export function getPorfile(id) {
  return request({
    url: '/manager/porfile/' + id,
    method: 'get'
  })
}

// 新增字体设置
export function addPorfile(data) {
  return request({
    url: '/manager/porfile',
    method: 'post',
    data: data
  })
}

// 修改字体设置
export function updatePorfile(data) {
  return request({
    url: '/manager/porfile',
    method: 'put',
    data: data
  })
}

// 删除字体设置
export function delPorfile(id) {
  return request({
    url: '/manager/porfile/' + id,
    method: 'delete'
  })
}
