import request from '@/utils/request'

// 查询系统更新列表
export function listSystemupdate(query) {
  return request({
    url: '/manager/systemupdate/list',
    method: 'get',
    params: query
  })
}

// 查询系统更新详细
export function getSystemupdate(uuid) {
  return request({
    url: '/manager/systemupdate/' + uuid,
    method: 'get'
  })
}

// 新增系统更新
export function addSystemupdate(data) {
  return request({
    url: '/manager/systemupdate',
    method: 'post',
    data: data
  })
}

// 修改系统更新
export function updateSystemupdate(data) {
  return request({
    url: '/manager/systemupdate',
    method: 'put',
    data: data
  })
}

// 删除系统更新
export function delSystemupdate(uuid) {
  return request({
    url: '/manager/systemupdate/' + uuid,
    method: 'delete'
  })
}


// 上传文件
export function uploadFileinfo(data) {
  return request({
    url: '/file/upload',
    method: 'post',
    data: data
  })
}
