import request from '@/utils/request'

// 查询评价内容列表
export function listAssessatr(query) {
  return request({
    url: '/manager/assessatr/list',
    method: 'get',
    params: query
  })
}

// 查询评价内容详细
export function getAssessatr(uuid) {
  return request({
    url: '/manager/assessatr/' + uuid,
    method: 'get'
  })
}

// 新增评价内容
export function addAssessatr(data) {
  return request({
    url: '/manager/assessatr',
    method: 'post',
    data: data
  })
}

// 修改评价内容
export function updateAssessatr(data) {
  return request({
    url: '/manager/assessatr',
    method: 'put',
    data: data
  })
}

// 删除评价内容
export function delAssessatr(uuid) {
  return request({
    url: '/manager/assessatr/' + uuid,
    method: 'delete'
  })
}
