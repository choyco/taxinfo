import request from '@/utils/request'

// 查询员工管理列表
export function listPicturefile(query) {
  return request({
    url: '/manager/picturefile/list',
    method: 'get',
    params: query
  })
}

// 查询员工管理详细
export function getPicturefile(id) {
  return request({
    url: '/manager/picturefile/' + id,
    method: 'get'
  })
}

// 新增员工管理
export function addPicturefile(data) {
  return request({
    url: '/manager/picturefile',
    method: 'post',
    data: data
  })
}

// 修改员工管理
export function updatePicturefile(data) {
  return request({
    url: '/manager/picturefile',
    method: 'put',
    data: data
  })
}

// 删除员工管理
export function delPicturefile(id) {
  return request({
    url: '/manager/picturefile/' + id,
    method: 'delete'
  })
}
