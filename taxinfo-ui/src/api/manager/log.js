import request from '@/utils/request'

// 查询日志信息列表
export function listLog(query) {
  return request({
    url: '/manager/log/list',
    method: 'get',
    params: query
  })
}

// 查询日志信息详细
export function getLog(uuid) {
  return request({
    url: '/manager/log/' + uuid,
    method: 'get'
  })
}

// 新增日志信息
export function addLog(data) {
  return request({
    url: '/manager/log',
    method: 'post',
    data: data
  })
}

// 修改日志信息
export function updateLog(data) {
  return request({
    url: '/manager/log',
    method: 'put',
    data: data
  })
}

// 删除日志信息
export function delLog(uuid) {
  return request({
    url: '/manager/log/' + uuid,
    method: 'delete'
  })
}
