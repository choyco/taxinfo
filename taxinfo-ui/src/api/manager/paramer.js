import request from '@/utils/request'

// 查询系统IP配置列表
export function listParamer(query) {
  return request({
    url: '/manager/paramer/list',
    method: 'get',
    params: query
  })
}

// 查询系统IP配置详细
export function getParamer(uuid) {
  return request({
    url: '/manager/paramer/' + uuid,
    method: 'get'
  })
}

// 新增系统IP配置
export function addParamer(data) {
  return request({
    url: '/manager/paramer',
    method: 'post',
    data: data
  })
}

// 修改系统IP配置
export function updateParamer(data) {
  return request({
    url: '/manager/paramer',
    method: 'put',
    data: data
  })
}

// 删除系统IP配置
export function delParamer(uuid) {
  return request({
    url: '/manager/paramer/' + uuid,
    method: 'delete'
  })
}
