import request from '@/utils/request'

// 查询设备管理列表
export function listEmployee(query) {
  return request({
    url: '/manager/employee/list',
    method: 'get',
    params: query
  })
}

// 查询设备管理详细
export function getEmployee(id) {
  return request({
    url: '/manager/employee/' + id,
    method: 'get'
  })
}

// 新增设备管理
export function addEmployee(data) {
  return request({
    url: '/manager/employee',
    method: 'post',
    data: data
  })
}

// 修改设备管理
export function updateEmployee(data) {
  return request({
    url: '/manager/employee',
    method: 'put',
    data: data
  })
}

// 删除设备管理
export function delEmployee(id) {
  return request({
    url: '/manager/employee/' + id,
    method: 'delete'
  })
}

// 查询设备管理，视频下拉
export function queryVideoSelect(query) {
  return request({
    url: '/manager/fileinfo/queryVideoSelect',
    method: 'get',
    params: query
  })
}
