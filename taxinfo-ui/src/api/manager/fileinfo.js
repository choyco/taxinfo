import request from '@/utils/request'

// 查询上传管理列表
export function listFileinfo(query) {
  return request({
    url: '/manager/fileinfo/list',
    method: 'get',
    params: query
  })
}

// 查询上传管理详细
export function getFileinfo(uuid) {
  return request({
    url: '/manager/fileinfo/' + uuid,
    method: 'get'
  })
}

// 新增上传管理
export function addFileinfo(data) {
  return request({
    url: '/manager/fileinfo',
    method: 'post',
    data: data
  })
}

// 修改上传管理
export function updateFileinfo(data) {
  return request({
    url: '/manager/fileinfo',
    method: 'put',
    data: data
  })
}

// 删除上传管理
export function delFileinfo(uuid) {
  return request({
    url: '/manager/fileinfo/' + uuid,
    method: 'delete'
  })
}


// 上传文件
export function uploadFileinfo(data) {
  return request({
    url: '/file/upload',
    method: 'post',
    data: data
  })
}
